from dotenv import load_dotenv
from langchain.llms import OpenAI
{%- if cookiecutter.llm_provider == "Azure OpenAI" %}
from langchain.llms import AzureOpenAI
{%- endif %}
{%- if cookiecutter.use_vectorstore == 'FAISS' %}
from langchain.vectorstores import FAISS
{%- endif %}
from langchain.chat_models import ChatOpenAI
{%- if cookiecutter.use_vectorstore == 'Cohere' %}
from langchain.embeddings import CohereEmbeddings
{%- endif %}
from langchain.callbacks import get_openai_callback
from langchain.memory import ConversationBufferMemory
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.chains import ConversationalRetrievalChain
from langchain.chains.question_answering import load_qa_chain
from langchain.text_splitter import RecursiveCharacterTextSplitter